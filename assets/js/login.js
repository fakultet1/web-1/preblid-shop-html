//kontrole za unos podataka

//------------------------LOGIN--------------------------------
$(document).on('click', '#loginButton', function () {
	console.log("onClick");
    var email = $('#inputEmail').val();
    var password = $('#inputPassword').val();
    if (email == null || email == "") {
        Swal.fire({
            title: 'Molimo unesite e-mail',
            buttonsStyling: false,
            customClass: {
                confirmButton: "btn btn-success"
            }
        })
    } else if (password == null || password == "") {
        Swal.fire({
            title: 'Molimo unesite lozinku !',
            buttonsStyling: false,
            customClass: {
                confirmButton: "btn btn-success" 
            }
        })
    } else {
        login();
    }
})


//-------------------------------REGISTRACIJA- ovo mi ne treba jer je registracija korisnika u forms.js i odmah nakon toga poziv i spremanje korisnika u bazu-----------------------------------------

$(document).on('click', '#spremiKor', function () {
	console.log("onClick");
    var ime = $ ('#ime').val();
    var prezime = $ ('#prezime').val();
    var email = $('#email').val();
    var password = $('#password').val();
    var ulica = $('#ulica').val();
    var kucBr = $('#kucBr').val();
    var grad = $('#grad').val();
    var pozmob = $('pozmob').val();
    var brmob = $('brmob').val();
    if (email == null || email == "") {
        Swal.fire({
            title: 'Molimo unesite e-mail !',
            buttonsStyling: false, 
            customClass: {
                confirmButton: "btn btn-success"
            }
        })
    } else if (password == null || password == "") {
        Swal.fire({
            title: 'Molimo unesite lozinku ! ',
            buttonsStyling: false,
            customClass: {
                confirmButton: "btn btn-success" 
            }
        })
    } else {
        login();
    }
})


//------------------------------------------------------------------------------------------

function login() {
	console.log("Uso u login");
    $.ajax({
        type: 'POST',
        url: url,
        data: {"projekt": projekt, 
               "procedura": "p_login", 
               "username": $('#inputEmail').val(), 
               "password": $('#inputPassword').val()
            },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcod = jsonBody.h_errcod;
            var message = jsonBody.h_message;
        
            if (message == null || message == "", errcod == null || errcod == 0) {
                $("#container").html('');
                 refresh(); 
            } else {
                Swal.fire(message + '.' + errcod);
            }
            
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: false

    });
}

function logout() {
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": projekt, 
                "procedura": "p_logout" 
            },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;

            if (message == null || message == "" || errcode == null) {
                Swal.fire("Greška u obradi podataka, molimo pokušajte ponovno!");
            } else {
                Swal.fire(message + '.' + errcode);
                
            }
            $("#container").html('');
            $("#podaci").html('');
            refresh();
           
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true
    });
}


