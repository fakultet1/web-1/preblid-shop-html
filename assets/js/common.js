//------konstante------------------------------------------------
var url = "https://dev.vub.zone/sandbox/router.php";
var projekt = "p_nkrivanek";
var perPage = 10;

//------hendlanje link button-a----------------------------------
$("#loginBtn").click(function () {
    $("#container").html(loginForm);
});



$("#login").click(function () {
    $("#container").html(loginForm());
});


$("#naslovna").click(function () {
    $("#container").html(indexForm());
});


$("#onama").click(function () {
    $("#container").html(aboutForm());
});


$("#registracija").click(function () {
    $("#container").html(regForm());
});


$("#kontakt").click(function () {
    $("#container").html(kontaktForm());
});

$("#logoutBtn").click(function () {
    logout();
});

$("#trgovina").click(function () {
   showProizvodi();
});

$("#spremiKor").click(function () {
    $("#container").html(insertForm());
});


//------------refersh-------------------------------------------------
$(function () {
    refresh();
});

//------------refersh--------------------------------------------------
function refresh() {
    console.log("refresh")
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": "p_nkrivanek", 
                "procedura": "p_refresh" 
              },
        success: function (data) {
            console.log("Success")
            console.log(data)
            var jsonBody = JSON.parse(data);
            if (jsonBody.h_errcode !== 999){
                var podaci = 'ID:' + jsonBody.ID + ', Ime i prezime: ' + jsonBody.ime + ' ' + jsonBody.prezime + ', Email:' + jsonBody.email;
                $("#podaci").html(podaci);
                $("#container").html(indexForm());
                
            } else{
                console.log("Not Success")
                $("#container").html(loginForm);
            } 
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}

//----------------------------------------------------------------
function pagination(pageNmb, perPage, count) {
    //ne treba prikazivati ništa
    if (count < perPage) {
        return '';
    } else {
        var quotient = Math.ceil(count / perPage);
    }
    var next = pageNmb + 1;
    var prev = pageNmb - 1;
    var pagination = '<div class="float-right pagination">';

    //treba prikazati previous
    if (pageNmb > 1) {
        pagination += '<ul class="pagination"><li class="page-item "><a class="page-link" onclick="showKorisnici(' + prev + ')" href="javascript:void(0)">‹</a></li>';
    }

    for (i = pageNmb; i < pageNmb + 8; i++) {
        pagination += '<li class="page-item"><a class="page-link" onclick="showKorisnici(' + i + ')" href="javascript:void(0)">' + i + '</a></li>';
    }

    pagination += '<li class="page-item"><a class="page-link"  href="javascript:void(0)">...</a></li>';

    pagination += '<li class="page-item"><a class="page-link" onclick="showKorisnici(' + quotient + ')" href="javascript:void(0)">' + quotient + '</a></li>';

    pagination += '<li class="page-item"><a class="page-link" onclick="showKorisnici(' + next + ')" href="javascript:void(0)">›</a></li>';
    pagination += '</ul></div>';
    return pagination;
}
//-----------ajaxSetup-------------------------------------------------------
$.ajaxSetup({
    xhrFields: {
        withCredentials: true
    }
});