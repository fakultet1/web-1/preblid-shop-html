//forma za prijavu
function loginForm(){
          var output = '<section class="container py-5"><div class="row pt-5 pb-3">';
          output +='<div class="col-lg-6 m-auto"></div><h1 class="h1">Prijava korisnika</h1>';
              output += '<p> Molim vas ispunite obrazac kako bi ste se prijavili</p>';
              output += '<hr>';
                   
              output +=  '<div class="mb-3 mt-3">';
              output += '<label for="inputEmail" class="form-label">';
              output+='<b>Email:</b></label><input type="email" class="form-control" ';
              output+='placeholder="Unesite email" name="inputEmail" id="inputEmail" required>';
              output+='</div><div class="mb-3"><label for="inputPassword" class="form-label">';
              output+='<b>Lozinka:</b></label><input type="password" class="form-control" id="inputPassword" placeholder="Unesite lozinku" name="inputPassword">';
              output+='</div><button id="loginButton" name="loginButton" class="btn btn-success">Pošalji</button></div></div>';
              return output;
}

//forma za stranicu O nama
function aboutForm(){
    var output = '<section class="bg-dark py-5"><div class="container"><div class="row align-items-center py-5"><div class="col-md-8 text-white">';
        output += ' <h1>O nama</h1>';
        output += ' <p>Prebild je lanac trgovina osnovan 2020 godine.<br>Osnovač i direktor je Nikolina Krivanek.</br> ';
        output += 'Kao veliki ljubitelji tehnologije i računalne opreme pružamo Vam razne kategorije i vrste. ';
        
        output += ' Prebild tim stalno radi na usavršavanju kako bi Vam mogli ponuditi najbolju kvalitetu za Vaš  novac.<br>';
        output +='Od samog početka Prebild je osvojio srca velikog broja malih i srednjih tvrtki te pojedinaca';
        output +=' kroz požrtvovan rad i profesionalan pristup poslu.</br>';
        output += ' <br> Ako tražite ozbiljnog partnera na pravom ste mjestu.</br>  </p>';
        output +=' </div>';
        output +=' <div class="col-md-4">';
        output +=' <img src="assets/img/infopr.png" alt="About Hero">';
        output +='  </div> </div> </div> </section>';
        return output;
}

//forma za naslovnu stranicu 
function  indexForm(){
    var output = '<section class="bg-dark py-5"><div class="container"><div id="template-mo-zay-hero-carousel" class="carousel slide" data-bs-ride="carousel">';
        output += '<div class="carousel-inner">';
        output += ' <div class="carousel-item active">';
        output += '<div class="container">';
        output += '<div class="row p-5">';
        output += ' <div class="mx-auto col-md-8 col-lg-6 order-lg-last">';
        output += ' <img class="img-fluid" src="./assets/img/slikashop.jpg" alt="">';
        output += ' </div> </div></div> </div> </div> </div> </section>';
        return output;
}

//forma za registraciju
function regForm(){
 var output = '<section class="container py-5">';
    output +='<div class="row pt-5 pb-3">';
    output += ' <div class="col-lg-6 m-auto">';
    output += '<h1 class="h1">Registracija korisnika</h1>';
    output += '<form>';
    output += ' <p> Molim vas ispunite obrazac kako bi ste se registrirali</p>';
    output += ' <hr><div class="mb mb-3 mt-3">';
    output += ' <label for="ime" class="form-label"><b>Ime:</b></label>';
    output += ' <input type="text" class="form-control" placeholder="Unesite ime" name="ime" id="inputime" required>';
    output += '</div> <div class="mb mb-3">';
    output += '<label for="prezime" class="form-label"><b>Prezime:</b></label>';
    output += '<input type="text" class="form-control" placeholder="Unesite prezime" name="prezime" id="inputprezime" required>';
    output += ' </div> <div class="mb-3 mt-3">';
    output +=' <label for="email" class="form-label"><b>Email:</b></label>';
    output += ' <input type="email" class="form-control" placeholder="Unesite email" name="email" id="inputemail" required>';
    output += ' </div><div class="mb-3">';
    output += '<label for="pwd" class="form-label"><b>Lozinka:</b></label>';
    output += ' <input type="password" class="form-control" id="inputpwd" placeholder="Unesite lozinku" name="pswd">';
    output += '</div><div class="mb-3"> <label for="ulica" class="form-label"><b>Ulica:</b></label>';
    output += ' <input type="text" class="form-control" id="inputulica" placeholder="Unesite ulicu" name="ulica">';
    output += ' </div> <div class="mb-3">';
    output +=' <label for="kucBr" class="form-label"><b>Kućni broj:</b></label>';
    output +='<input type="text" class="form-control" id="inputkucBr" placeholder="Unesite kućni broj" name="kucBr">';
    output +=' </div><div class="mb-3">';
    output += ' <label for="grad" class="form-label"><b>Grad:</b></label>';
    output +='<input type="text" class="form-control" id="inputgrad" placeholder="Unesite grad" name="grad">';
    output +=' </div><div class="mb-3">';
    output +='<label for="pozmob" class="form-label"><b>Pozivni broj mobitela:</b></label>';
    output += ' <input type="number" class="form-control" id="inputpozMob" placeholder="Unesite broj:" name="pozmob">';
    output +='<label for="brmob" class="form-label"><b> Broj mobitela:</b></label>';
    output += ' <input type="number" class="form-control" id="inputbrMob" placeholder="Unesite broj mobitela:" name="brmob">'
    output += ' </div> ';
    output += '</form> <button id=inputregBtn name=regButton class="btn btn-success">Pošalji</button> </div> </div>';
    return output;

}   

//kontakt forma
function kontaktForm(){
    var output ='<section class="bg-dark py-5">';
    output +=' <div class="container">';
     output +='  <div class="row align-items-center py-5">';
        output +='<div class="col-md-8 text-white">'  ;
        output +='<h1>Kontakti</h1>';
        output +=' <p> Poslovnice:<br> </br>';
        output +='Poslovnica Prebild 1: <br>';
        output +=' telefon: 0987651234;';
        output += 'adresa:Bjelovarska ulica 9,10000 Zagreb,Hrvatska';
        output +='</br><br></br>';
        output +='Poslovnica Prebild 2:<br>';
        output +=' telefon:0981234765';
        output +=' adresa:Tratinska ulica 4,10000 Zagreb,Hrvatska</br><br></br>';
        output +='Poslovnica Prebild 3:<br>';
        output +=' telefon:0984321567';
        output +=' adresa: Gudovačka cesta 41,43000 Bjelovar,Hrvatska</br>';
        output +='  </p> </div> <div class="col-md-4">';
        output += '<img src="assets/img/infopr.png" alt="About Hero">';
        output += ' </div></div></div></section>';
       return output; 
}
     

               
 /*   ovo mi ne treba-nije obrisano za svaki slucaj ako zatreba               
function insertForm(page) {
    var output = '<table class="table table-hover"><tbody>';
    output += '<tr><th scope="col">ime</th><td><input type="text" id="IME"></td></tr>';
    output += '<tr><th scope="col">prezime</th><td><input type="text" id="PREZIME"></td></tr>';
    output += '<tr><th scope="col">email</th><td><input type="email" id="EMAIL"></td></tr>';
    output += '<tr><th scope="col">password</th><td><input type="text" id="PASSWORD"></td></tr>';
    output += '<tr><th scope="col">ulica</th><td><input type="text" id="ULICA"></td></tr>';
    output += '<tr><th scope="col">kucbr</th><td><input type="text" id="KUCBR"></td></tr>';
    output += '<tr><th scope="col">dodkucbr</th><td><input type="text" id="DODKUCBR"></td></tr>';
    output += '<tr><th scope="col">grad</th><td><input type="text" id="GRAD"></td></tr>';
    output += '<tr><th scope="col">pozmob</th><td><input type="text" id="POZMOB"></td></tr>';
    output += '<tr><th scope="col">brmob</th><td><input type="text" id="BRMOB"></td></tr>';
    output += '</table>';
    output += '<button type="button" class="btn btn-warning" id="spremiKor">Spremi <i class="fas fa-save"></i></button> ';
    output += '<button type="button" class="btn btn-success" onclick="saveKor(' + page + ')">Odustani <i class="fas fa-window-close"></i></button>';
    $("#container").html(output);
}*/
                
               
 /* registracija korisnika i unos podataka u bazu - funkcija osluskuje i ceka da se stisne  inoutregBtn pa da se okine funkcija i da se podaci spremaju u bazu */              

$(document).on('click', '#inputregBtn', function () {
    console.log("uslo u save");
    var IME = $('#inputime').val();
    var PREZIME = $('#inputprezime').val();
    var EMAIL = $('#inputemail').val();
    var PASSWORD = $('#inputpwd').val();
    var ULICA = $('#inputulica').val();
    var KUCBR = $('#inputkucBr').val();
    var GRAD = $('#inputgrad').val();
    var POZMOB = $ ('#inputpozMob').val();
    var BRMOB = $ ('#inputbrMob').val();

    var ID = $('#ID').val();

    if (IME == null || IME == "") {
        Swal.fire('Molimo unesite ime korisnika');
    } else if (PREZIME == null || PREZIME == "") {
        Swal.fire('Molimo unesite prezime korisnika');
    } else if (EMAIL == null || EMAIL == "") {
        Swal.fire('Molimo unesite email korisnika');
    } else if (PASSWORD == null || PASSWORD== "") {
        Swal.fire('Molimo unesite lozinku');
    } else if (ULICA == null || ULICA == "") {
        Swal.fire('Molimo unesite ulicu');
    } else if (KUCBR == null || KUCBR == "") {
        Swal.fire('Molimo unesite kucni broj');
    } else if (GRAD == null || GRAD == "") {
        Swal.fire('Molimo unesite grad');
    }
    else if (typeof POZMOB !='number' ) {
        Swal.fire('Pozivni broj ima 3 znamenke npr.091');
    }else if (POZMOB == null || POZMOB == "") {
        Swal.fire('Molimo unesite pozivni broj mobitela');
    }
    else if (typeof BRMOB !='number') {
        Swal.fire('Broj mobitela mora biti broj !');
    }
    else if (BRMOB == null || BRMOB == "") {
        Swal.fire('Molimo unesite broj mobitela ! ');
    }
     else {
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                "projekt": projekt,
                "procedura": "p_spremi_korisnika",
                "ID": ID,
                "IME": IME,
                "PREZIME": PREZIME,
                "EMAIL": EMAIL,
                "PASSWORD": PASSWORD,
                "ULICA": ULICA,
                "KUCBR": KUCBR,
                "GRAD": GRAD,
                "POZMOB": POZMOB,
                "BRMOB": BRMOB
            },
            success: function (data) {
                var jsonBody = JSON.parse(data);
                var errcode = jsonBody.h_errcod;
                var message = jsonBody.h_message;
                console.log(data);

                if ((message == null || message == "") && (errcode == null || errcode == 0)) {
                    Swal.fire('Uspješno ste se registrirali');
                } else {
                    Swal.fire(message + '.' + errcode);
                }
               // refresh();
               loginForm();
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            },
            async: true
        });
    }
})

               
                    
               
                
                    
                   
               

               
            
       
  