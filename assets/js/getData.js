

/* prikaz svih proizvoda kada se klikne na trgovinu*/
function showProizvodi(ID, page) {
    var tablica = '<div class="row">';
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": projekt, "procedura": "p_get_proizvod", "ID": ID },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;

            if (message == null || message == "", errcode == null || errcode == 0) {
                $.each(jsonBody.data, function (k, v) {
                    tablica += '<div class="col-md-4">';
                    tablica += ' <div class="card mb-4 product-wap rounded-0">';
                    tablica += '<div class="card rounded-0">';
                    tablica += '<img class="card-img rounded-0 img-fluid" src="assets/img/' + v.ID + '.jpg">';
                    tablica += ' <div class="card-img-overlay rounded-0 product-overlay d-flex align-items-center justify-content-center">';
                    tablica += ' <ul class="list-unstyled">';
                    tablica += ' <li><a class="btn btn-success text-white mt-2" href="#" onclick="showProizvod(' + v.ID + ');"><i class="far fa-eye"></i></a></li>';
                    tablica += '  </ul> </div> </div>';
                    tablica += ' <div class="card-body">';
                    tablica += '<a href="#" onclick="showProizvod(' + v.ID + ');" class="h3 text-decoration-none">' + v.BRAND + ' ' + v.NAZIV + '</a>';
                    tablica += '<ul class="w-100 list-unstyled d-flex justify-content-between mb-0">';
                    tablica += ' <li class="pt-2">';
                    tablica += ' <span class="product-color-dot color-dot-red float-left rounded-circle ml-1"></span>';
                    tablica += ' <span class="product-color-dot color-dot-blue float-left rounded-circle ml-1"></span>';
                    tablica += ' <span class="product-color-dot color-dot-black float-left rounded-circle ml-1"></span>';
                    tablica += ' <span class="product-color-dot color-dot-light float-left rounded-circle ml-1"></span>';
                    tablica += ' <span class="product-color-dot color-dot-green float-left rounded-circle ml-1"></span>';
                    tablica += ' </li> </ul> <p class="text-center mb-0">' + v.CIJENA + ' hrk</p>';
                    tablica += ' </div> </div> </div>';
                    /* tablica += '<button type="button" class="btn btn-warning" id="spremiProiz">Spremi <i class="fas fa-save"></i></button> ';
                     tablica += '<button type="button" class="btn btn-success" onclick="showProizvod(' + page + ')">Odustani <i class="fas fa-window-close"></i></button>';*/
                });
                tablica += '</div>';
                $("#container").html(tablica);
            } else {
                if (errcode == 999) {
                    $("#container").html(loginForm);
                } else {
                    Swal.fire(message + '.' + errcode);
                }
            }
            //refresh();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}

/*   funkcija za prikaz jednog proizvoda */

function showProizvod(ID, page) {
    var tablica = '<table class="table table-bordered table-hover" ><tbody>';
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": projekt, "procedura": "p_get_proizvod", "ID": ID },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;

            if (message == null || message == "", errcode == null || errcode == 0) {
                $.each(jsonBody.data, function (k, v) {
                    tablica += '<tr><th scope="col">ID</th><td><input type="text" id="ID" value="' + v.ID + '" readonly></td></tr>';
                    tablica += '<tr><th scope="col">IDVRPROIZ</th><td><input type="text" id="IDVRPROIZ" value="' + v.IDVRPROIZ + '" readonly></td></tr>';
                    tablica += '<tr><th scope="col">BRAND</th><td><input type="text" id="BRAND" value="' + v.BRAND + '"></td></tr>';
                    tablica += '<tr><th scope="col " >OPIS</th><td><input type="text"  id="OPIS" value="' + v.OPIS + '"></td></tr>';
                    tablica += '<tr><th scope="col">NAZIV</th><td><input type="text" id="NAZIV" value="' + v.NAZIV + '"></td></tr>';
                    tablica += '<tr><th scope="col">CIJENA</th><td><input type="number" id="CIJENA" value="' + v.CIJENA + '"></td></tr>';
                    tablica += '</table>';
                    tablica += '<button type="button" class="btn btn-success" id="spremiProiz">Spremi <i class="fas fa-save"></i></button> ';
                    tablica += '<button type="button" class="btn btn-danger" id="delProiz">Obrisi <i class="fas fa-window-close"></i></button>';
                });
                $("#container").html(tablica);
            } else {
                if (errcode == 999) {
                    $("#container").html(loginForm);
                } else {
                    Swal.fire(message + '.' + errcode);
                }
            }
            //refresh();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}


// spremanje/ mjenjanje proizvoda -- gumb save proizvod 

$(document).on('click', '#spremiProiz', function () {
    var ID = $('#ID').val();
    var IDVRPROIZ = $('#IDVRPROIZ').val();
    var BRAND = $('#BRAND').val();
    var OPIS = $('#OPIS').val();
    var NAZIV = $('#NAZIV').val();
    var CIJENA = $('#CIJENA').val();


    if (IDVRPROIZ == null || IDVRPROIZ == "") {
        swal.fire('Molimo unesite ID vrste proizvoda');
    } else if (BRAND == null || BRAND == "") {
        swal.fire('Molimo unesite brand proizvoda');
    } else if (OPIS == null || OPIS == "") {
        swal.fire('Molimo unesite opis proizvoda');
    } else if (NAZIV == null || NAZIV == "") {
        swal.fire('Molimo unesite naziv proizvoda');
    }else if (typeof CIJENA !='number') {
        swal.fire('Cijena proizvoda mora biti broj !');
    }
    else if (CIJENA == null || CIJENA == "") {
        swal.fire('Molimo unesite cijenu proizvoda');
    }
     else {
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                "projekt": projekt,
                "procedura": "p_spremi_proizvod",
                "ID": ID,
                "IDVRPROIZ": IDVRPROIZ,
                "BRAND": BRAND,
                "OPIS": OPIS,
                "NAZIV": NAZIV,
                "CIJENA": CIJENA,

            },
            success: function (data) {
                var jsonBody = JSON.parse(data);
                var errcode = jsonBody.h_errcode;
                var message = jsonBody.h_message;
                console.log(data);

                if ((message == null || message == "") && (errcode == null || errcode == 0)) {
                    Swal.fire('Uspješno se unijeli proizvod !','success');

                } else {
                    Swal.fire(message);
                    if (errcode == 100) {
                        showProizvodi();
                    }
                }


            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            },
            async: true
        });
    }

})

// funkcija za brisanje proizvoda - delete button 

$(document).on('click', '#delProiz', function () {
    var ID = $('#ID').val();
    var IDVRPROIZ = $('#IDVRPROIZ').val();
    var BRAND = $('#BRAND').val();
    var OPIS = $('#OPIS').val();
    var NAZIV = $('#NAZIV').val();
    var CIJENA = $('#CIJENA').val();
    swal.fire({
        title: 'Želite li zaista obrisati proizvod?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: 'green',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Da, obriši proizvod!',
        cancelButtonText: 'Odustani!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    "projekt": projekt,
                    "procedura": "p_spremi_proizvod",
                    "AKCIJA": "DELETE",
                    "ID": ID,
                    "IDVRPROIZ": IDVRPROIZ,
                    "BRAND": BRAND,
                    "OPIS": OPIS,
                    "NAZIV": NAZIV,
                    "CIJENA": CIJENA

                },
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var errcode = jsonBody.h_errcode;
                    var message = jsonBody.h_message;
                    console.log(data);

                    if ((message == null || message == "") && (errcode == null || errcode == 0)) {
                        Swal.fire(
                            'Uspješno ',
                            'ste obrisali proizvod',
                            'info'
                        );
                    } else {
                        Swal.fire(message + '.' + errcode);
                    }
                    refresh();
                    showProizvodi();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true
            });
        }

    })
})


/*
function showProizvod(ID, page) {
    var tablica = '<table class="table table-hover"><tbody>';
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": projekt, "procedura": "p_get_proizvod", "ID": ID },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;

            if (message == null || message == "", errcode == null || errcode == 0) {
                $.each(jsonBody.data, function (k, v) {
                    tablica += '<tr><th scope="col">ID</th><td><input type="text" id="ID" value="' + v.ID + '" readonly></td></tr>';
                    tablica += '<tr><th scope="col">IDVRPROIZ</th><td><input type="text" id="ID" value="' + v.IDVRPROIZ + '" readonly></td></tr>';
                    tablica += '<tr><th scope="col">BRAND</th><td><input type="text" id="BRAND" value="' + v.BRAND + '"></td></tr>';
                    tablica += '<tr><th scope="col">OPIS</th><td><input type="text" id="OPIS" value="' + v.OPIS + '"></td></tr>';
                    tablica += '<tr><th scope="col">NAZIV</th><td><input type="text" id="NAZIV" value="' + v.NAZIV + '"></td></tr>';
                    tablica += '<tr><th scope="col">CIJENA</th><td><input type="text" id="CIJENA" value="' + v.CIJENA + '"></td></tr>';
                    tablica += '</table>';
                    tablica += '<button type="button" class="btn btn-warning" id="spremiProiz">Spremi <i class="fas fa-save"></i></button> ';
                    tablica += '<button type="button" class="btn btn-success" onclick="showProizvod(' + page + ')">Odustani <i class="fas fa-window-close"></i></button>';
                });
                $("#container").html(tablica);
            } else {
                if (errcode == 999) {
                    $("#container").html(loginForm);
                } else {
                    Swal.fire(message + '.' + errcode);
                }
            }
            //refresh();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}*/
